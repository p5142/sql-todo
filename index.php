<?php require "connection.php"?>
<!-- Establish connection to php my admin -->
<?php
if ($mysqli->connect_error) {
    die("Connection failed: " . $mysqli->connect_error);
}
?>
<!----------------------------------------------------------------ADD ITEMS TO: TO DO------------------------------------------------------------------------------->
<h1>ADD AN ITEM TO THE TABLE</h1>
<form method="post">
        Task: <input type="text" name="task_name"/>
        <br>
        <br>
        Task difficulty: <input type="number" name="difficulty_task"/>
        <br>
        <br>
        Assigned to: <input type="text" name="name_assigned_to"/>
        <br>
        <br>
        <input type="submit" name="submit" value="Submit"/>
</form>

<?php 
$name_task            =    !empty ($_REQUEST["task_name"]) ? $_REQUEST["task_name"] : NULL;
$task_difficulty      =    !empty ($_REQUEST["difficulty_task"]) ? (int) $_REQUEST["difficulty_task"] : NULL;
$assigned             =    !empty ($_REQUEST["name_assigned_to"]) ? $_REQUEST["name_assigned_to"] : NULL;
if(!empty($_REQUEST['task_name'] && $_REQUEST['difficulty_task'] && $_REQUEST['name_assigned_to'] ) === true){
    $sql2 = $mysqli->prepare("INSERT INTO list_of_todo (task, assigned_to, task_difficult) VALUES (?, ?, ?)");
    // assign variables
    $sql2->bind_param('ssi', $name_task, $assigned, $task_difficulty);
    $sql2->execute();
}
?>
<!----------------------------------------------------------------UPDATE ITEMS TO: TO DO------------------------------------------------------------------------------->
<h1>UPDATE AN ITEM IN THE TABLE</h1>
<form method="post">
        Task to assign: <input type="text" name="task_is"/>
        <br>
        <br>
        Assign this task to: <input type="text" name="new_assignment"/>
        <br>
        <br>
        <input type="submit" name="update" value="Update"/> 
</form>

<?php 
$assigned_task         =    !empty ($_REQUEST["task_is"]) ? $_REQUEST["task_is"] : NULL;
$assign_to_someone     =    !empty ($_REQUEST["new_assignment"]) ? $_REQUEST["new_assignment"] : NULL;
?>

<?php
if(!empty($_REQUEST['task_is'] && $_REQUEST['new_assignment']) === true){
    $sql3 = $mysqli->prepare("UPDATE list_of_todo SET assigned_to = '$assign_to_someone' WHERE task= ?");
    $sql3->bind_param('s', $assigned_task);
    $sql3->execute();
    // if ($mysqli->query($sql3) === TRUE) {
    //     echo "modified!!";
    // }
    // else {
    //     echo "Error: " . $sql3 . "<br>" . $mysqli->error;
    // }
}
?>
<!----------------------------------------------------------------DELETE IN TO DO------------------------------------------------------------------------------->
<h1>DELETE AN ITEM FROM THE TABLE</h1>
<form method="post">
        Delete the task(s) with the following name: <input name="erase"/>
        <br>
        <br>
        <input type="submit" name="delete" value="Delete"/> 
</form>

<?php
// DELETE FROM DATABASE
$delete_task           =    !empty ($_REQUEST["erase"]) ? $_REQUEST["erase"] : NULL;

if(!empty($_REQUEST['delete'])){
    $sql4 = $mysqli->prepare("DELETE FROM list_of_todo WHERE task= ?");
    $sql4->bind_param('s', $delete_task);
    $sql4->execute();
}
?>
<!----------------------------------------------------------------SHOW ALL ITEMS IN TO DO------------------------------------------------------------------------------->
<h1>TABLE RESULTS</h1>
<?php
// Query to get information from the database
$sql = "SELECT * FROM list_of_todo";
$result = $mysqli->query($sql);
// Show results of data
if ($result->num_rows > 0) {
// Show tasks in a table
    echo "<table>";
        while($row = $result->fetch_assoc()) {
            echo "<tr><td>" . "Task: " . " " . $row['task'] . " " . "Difficulty of:" . " " . $row['task_difficult'] .  " " . "Assigned to:" . " " . $row['assigned_to'] . "</td><td>";
        }
    echo "</table>";
} 
else {
    echo "0 results";
};
?>